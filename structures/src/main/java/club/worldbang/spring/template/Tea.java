package club.worldbang.spring.template;

public class Tea extends DrinkBeverage{

	@Override
	public void pourInCup() {
		System.out.println("放入茶叶");
	}

	@Override
	public void addOthers() {
		System.out.println("添加蜂蜜");
	}

}
