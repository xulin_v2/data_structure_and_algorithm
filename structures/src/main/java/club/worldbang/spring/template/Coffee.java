package club.worldbang.spring.template;

public class Coffee extends DrinkBeverage{

	@Override
	public void pourInCup() {
		System.out.println("咖啡倒入杯中");
	}

	@Override
	public void addOthers() {
		System.out.println("添加牛奶");
	}

}
