package club.worldbang.spring.template;

/**
 * 模板模式
 *冲饮料
 */
public abstract class DrinkBeverage {
	
	//模板流程不能被重写
	public final void create() {
		//烧水
		boilWater();
		//准备材料
		pourInCup();
		//冲泡
		brew();
		//辅料添加
		addOthers();
	}

	public void boilWater() {
		System.out.println("水烧开了....");
	}
	
	public abstract void pourInCup() ;

	public void brew() {
		System.out.println("将水冲入杯中");
	}

	public abstract void addOthers() ;

}
