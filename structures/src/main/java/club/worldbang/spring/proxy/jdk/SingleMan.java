package club.worldbang.spring.proxy.jdk;

/**
 * 单身人士
 * @author linxu
 *
 */
public class SingleMan implements Person {
	private String name = "小星星";
	private String sex = "女";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	@Override
	public void findLove() {
		System.out.println("我叫" + name + ",性别:" + sex + ",找对象的要求:");
		System.out.println("身高180cm");
		System.out.println("体重70kg");
		System.out.println("高富帅");
	}

}
