package club.worldbang.spring.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 媒婆
 * 
 * @author linxu 代理实现
 */
public class Matcher implements InvocationHandler {
	private Person target;

	// 获取被代理的人的对象
	public Object getInstance(Person target) {
		this.target = target;

		Class clazz = target.getClass();
		System.out.println("媒婆拿到的类型："+clazz);
		return Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(), this);
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("我是媒婆,你的性别为："+this.target.getSex());
		this.target.findLove();
		System.out.println("开始海选...");
		System.out.println("海选结束，如果觉得合适，进行下一步流程");
		
		
		return null;
	}

}
