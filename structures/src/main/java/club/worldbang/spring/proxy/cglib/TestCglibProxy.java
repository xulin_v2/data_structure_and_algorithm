package club.worldbang.spring.proxy.cglib;

public class TestCglibProxy {
	
	public static void main(String[] args) {
//		SingleMan findObject = new SingleMan();
//		findObject.findLove();
		
		SingleMan proxyHelp = (SingleMan) new WBMatcher().getInstance(SingleMan.class); 
		proxyHelp.findLove();
	}

}
