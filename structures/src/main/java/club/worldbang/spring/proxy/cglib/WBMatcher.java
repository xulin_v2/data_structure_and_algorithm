package club.worldbang.spring.proxy.cglib;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

/**
 * worldbang媒婆
 */
public class WBMatcher implements MethodInterceptor{
	
	public Object getInstance(Class clazz) {
		
		//通过反射机制实例化
		Enhancer enhancer = new Enhancer();
		enhancer.setSuperclass(clazz);
		enhancer.setCallback(this);
		
		return enhancer.create();
	}

	//同样做了字节码重组，对于使用API的用户来说是无感知的
	@Override
	public Object intercept(Object obj, Method method, Object[] arg2, MethodProxy proxy) throws Throwable {

		System.out.println("我是媒婆,来个要求，帮你找对象...");
		proxy.invokeSuper(obj,arg2);
		System.out.println("====================");
		System.out.println("开始海选...");
		System.out.println("====================");

		System.out.println("海选结束，如果觉得合适，进行下一步流程");
		
		return null;
	}

}
