package club.worldbang.spring.proxy.jdk;


/**
 * 相亲测试
 * 
 * @author linxu
 *
 */
public class TestJdkProxy {
	public static void main(String[] args) {
		// ======直接实现 Start======//
		// new SingleMan().findLove();
		// ======直接实现 End======//

		// ======代理实现 Start======//
		Person xxx = (Person) new Matcher().getInstance(new SingleMan());
		System.out.println(xxx.getClass());
		xxx.findLove();
		/*
		 实现原理： 
		 (1)直接拿到被代理对象的引用，然后拿到他的接口
		 (2)JDK反射重新生成一个类，同时实现我们所给代理对象的接口
		 (3)拿到了被代理对象的引用
		 (4)invoke方法，重新动态生成class字节码
		 (5)然后编译
		 */
		// ======代理实现 End======//


	}
}
