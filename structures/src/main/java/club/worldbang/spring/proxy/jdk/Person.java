package club.worldbang.spring.proxy.jdk;

/**
 * @author linxu
 *
 */
public interface Person {
	public String getSex();
	public String getName();
	void findLove();

}
