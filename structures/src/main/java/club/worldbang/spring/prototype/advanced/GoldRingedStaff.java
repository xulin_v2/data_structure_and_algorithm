package club.worldbang.spring.prototype.advanced;

import java.io.Serializable;

/**
 * 金箍棒
 */
public class GoldRingedStaff implements Serializable{

	private static final long serialVersionUID = -4973322890628589933L;
	
	private float height = 100;//长
	private float diameter = 10;//粗
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public float getDiameter() {
		return diameter;
	}
	public void setDiameter(float diameter) {
		this.diameter = diameter;
	}
	
	//变长
	public void grow() {
		this.diameter *=2;
		this.height *=2;
	}
	//变短
	public void shrink() {
		this.diameter /=2;
		this.height /=2;
	}
	
	
}
