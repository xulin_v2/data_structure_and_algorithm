package club.worldbang.spring.prototype.advanced;

import java.util.Date;

/**
 * 猴子
 */
public class Monkey {
	private int height;//身高
	private int weight;//体重
	private Date birthday;//生日
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	
}
