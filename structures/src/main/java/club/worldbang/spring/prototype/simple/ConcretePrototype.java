package club.worldbang.spring.prototype.simple;

import java.util.ArrayList;
import java.util.List;

public class ConcretePrototype implements Cloneable{
	
	private int age;
	
	private ArrayList<String> list ;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public ArrayList<String> getList() {
		return list;
	}

	public void setList(ArrayList<String> list) {
		this.list = list;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		ConcretePrototype cp = (ConcretePrototype) super.clone();
		cp.list = (ArrayList<String>) list.clone();
		return cp;
	}
}
