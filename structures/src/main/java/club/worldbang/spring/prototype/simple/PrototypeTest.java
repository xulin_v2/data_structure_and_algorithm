package club.worldbang.spring.prototype.simple;

import java.util.ArrayList;
import java.util.List;

public class PrototypeTest {
	
	public static void main(String[] args) {
		ConcretePrototype cp = new ConcretePrototype();
		cp.setAge(18);
		
		ArrayList<String> list = new ArrayList<>();
		list.add(1+"");
		
		cp.setList(list);
		try {
			//能够直接拷贝的内容类型只支持9种，8大基础类型String 即默认是 浅拷贝
			ConcretePrototype copy = (ConcretePrototype) cp.clone();
			System.out.println((copy == cp)+","+copy.getAge()+", list比较:"+(copy.getList() == cp.getList()));
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}

}
