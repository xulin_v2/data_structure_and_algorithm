package club.worldbang.spring.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MyListTest {

	public static void main(String[] args) {
		
		//策略模式
		List<Integer> numbers = new ArrayList<>();
		Collections.sort(numbers, new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				//处理逻辑不一样
				//0 -1 1
				return 0;
			}
		});
	}
}
