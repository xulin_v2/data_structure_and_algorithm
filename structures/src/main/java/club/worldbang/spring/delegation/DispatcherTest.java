package club.worldbang.spring.delegation;

public class DispatcherTest {
	
	public static void main(String[] args) {
		//貌似dispatcher在执行，实际将任务委派给A在执行
		//如：公司干活，大boss只会找对应的pm，不会找pm分配给对应工作的人员
		Dispatcher dispatcher = new Dispatcher(new TargetA());
		dispatcher.doing();
	}
	
}
