package club.worldbang.spring.delegation;

public class Dispatcher implements IDelegation {
	IDelegation exector;
	
	public Dispatcher(IDelegation exector) {
		this.exector =  exector;
	}
	
	@Override
	public void doing() {
		exector.doing();
	}

}
