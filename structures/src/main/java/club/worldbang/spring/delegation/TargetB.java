package club.worldbang.spring.delegation;

public class TargetB implements IDelegation {

	@Override
	public void doing() {
		System.out.println("员工B执行任务...");
	}

}
