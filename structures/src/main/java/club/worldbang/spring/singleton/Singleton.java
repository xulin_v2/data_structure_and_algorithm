package club.worldbang.spring.singleton;
/**
 * 单例模式，懒汉加载
 * 调用的时候再分配空间，不调用就不分配
 * 线程安全，独一份不管怎么操作
 */
public class Singleton {

	//1、先声明一个静态内部类 private私有、static静态加载全局唯一
	private static class LazyHandler{
		//final 防止误操作，如：利用cglib代理模式，重新赋值
		private static final Singleton INSTANCE = new Singleton();
	}
	
	//2、默认构造私有
	private Singleton() {
	}
	
	//3、静态方法获取实例，final修饰确保不被覆盖
	public static final Singleton getInstance() {
		return LazyHandler.INSTANCE;
	}
}
