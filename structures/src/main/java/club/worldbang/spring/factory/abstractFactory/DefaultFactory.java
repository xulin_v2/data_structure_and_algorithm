package club.worldbang.spring.factory.abstractFactory;

import club.worldbang.spring.factory.Car;

public class DefaultFactory extends AbstractFactory{

	AudiFactory defaultFactory  = new AudiFactory();
	
	@Override
	public Car getCar() {
		return defaultFactory.getCar();
	}

}
