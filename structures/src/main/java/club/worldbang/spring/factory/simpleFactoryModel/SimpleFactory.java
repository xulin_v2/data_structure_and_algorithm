package club.worldbang.spring.factory.simpleFactoryModel;

import club.worldbang.spring.factory.Audi;
import club.worldbang.spring.factory.Benz;
import club.worldbang.spring.factory.Bmw;
import club.worldbang.spring.factory.Car;

/**
 *简单工厂 
 */
public class SimpleFactory {
	//实现统一管理
	public Car getCar(String name) {
		if("BMW".equalsIgnoreCase(name)) {
			return new Bmw();
		}else if("Audi".equalsIgnoreCase(name)) {
			return new Audi();

		}else if("Benz".equalsIgnoreCase(name)) {
			return new Benz();
		}else {
			System.out.println("这个牌子不存在");
			return null;
		}
	}
}
