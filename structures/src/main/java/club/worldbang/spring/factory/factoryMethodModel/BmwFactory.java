package club.worldbang.spring.factory.factoryMethodModel;

import club.worldbang.spring.factory.Bmw;
import club.worldbang.spring.factory.Car;

public class BmwFactory implements Factory {

	@Override
	public Car getCar() {
		// TODO Auto-generated method stub
		return new Bmw();
	}

}
