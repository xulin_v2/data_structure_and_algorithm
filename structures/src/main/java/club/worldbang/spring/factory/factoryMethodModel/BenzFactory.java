package club.worldbang.spring.factory.factoryMethodModel;

import club.worldbang.spring.factory.Benz;
import club.worldbang.spring.factory.Car;

public class BenzFactory implements Factory{

	@Override
	public Car getCar() {
		// TODO Auto-generated method stub
		return new Benz();
	}

}
