package club.worldbang.spring.factory.abstractFactory;

public class AbstractFactoryTest {
	
	public static void main(String[] args) {
		DefaultFactory factory = new DefaultFactory();
		System.out.println(factory.getCar().getName());
		System.out.println(factory.getCar("bmw").getName());
	}

}
