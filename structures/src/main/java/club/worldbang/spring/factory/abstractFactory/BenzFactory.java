package club.worldbang.spring.factory.abstractFactory;

import club.worldbang.spring.factory.Benz;
import club.worldbang.spring.factory.Car;

public class BenzFactory extends AbstractFactory{

	@Override
	public Car getCar() {
		// TODO Auto-generated method stub
		return new Benz();
	}

}
