package club.worldbang.spring.factory.abstractFactory;

import club.worldbang.spring.factory.Audi;
import club.worldbang.spring.factory.Car;

public class AudiFactory extends AbstractFactory {

	@Override
	public Car getCar() {
		// TODO Auto-generated method stub
		return new Audi();
	}

}
