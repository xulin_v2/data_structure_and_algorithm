package club.worldbang.spring.factory.abstractFactory;

import club.worldbang.spring.factory.Car;

public abstract class AbstractFactory {
	protected abstract Car getCar();
	
	public Car getCar(String name) {
		if("BMW".equalsIgnoreCase(name)) {
			return new BmwFactory().getCar();
		}else if("Audi".equalsIgnoreCase(name)) {
			return new AudiFactory().getCar();

		}else if("Benz".equalsIgnoreCase(name)) {
			return new BenzFactory().getCar();
		}else {
			System.out.println("这个牌子不存在");
			return null;
		}
	}
	
}
