package club.worldbang.spring.factory.abstractFactory;

import club.worldbang.spring.factory.Bmw;
import club.worldbang.spring.factory.Car;

public class BmwFactory extends AbstractFactory {

	@Override
	public Car getCar() {
		// TODO Auto-generated method stub
		return new Bmw();
	}

}
