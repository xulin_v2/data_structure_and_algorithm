package club.worldbang.spring.factory.factoryMethodModel;

import club.worldbang.spring.factory.Audi;
import club.worldbang.spring.factory.Car;

public class AudiFactory implements Factory {

	@Override
	public Car getCar() {
		// TODO Auto-generated method stub
		return new Audi();
	}

}
