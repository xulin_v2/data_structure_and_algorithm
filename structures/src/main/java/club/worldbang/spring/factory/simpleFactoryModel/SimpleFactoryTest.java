package club.worldbang.spring.factory.simpleFactoryModel;

import club.worldbang.spring.factory.Car;

public class SimpleFactoryTest {
	
	public static void main(String[] args) {
		//消费者，只负责消费产品，获得信息
		Car car = new SimpleFactory().getCar("BMW");
		System.out.println(car.getName());
		
	}

}
