package club.worldbang.spring.factory.factoryMethodModel;

import club.worldbang.spring.factory.Car;

public interface Factory {
	Car getCar();
}
