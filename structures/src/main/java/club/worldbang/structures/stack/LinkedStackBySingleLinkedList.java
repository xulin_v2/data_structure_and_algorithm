package club.worldbang.structures.stack;

import java.io.Serializable;

import club.worldbang.structures.linkedList.singleLinked.SingleILinkedList;

//单链表实现链式栈
public class LinkedStackBySingleLinkedList<T> implements Serializable,Stack<T>{

	private static final long serialVersionUID = -7296189870266647381L;
	SingleILinkedList<T> linkedList;
	
	public LinkedStackBySingleLinkedList() {
		linkedList = new SingleILinkedList<>();
	}

	@Override
	public boolean isEmpty() {
		return linkedList.isEmpty();
	}

	@Override
	public void push(T data) {
		linkedList.add(data);
	}

	@Override
	public T peek() {
		if(isEmpty()) {
			throw new RuntimeException("stack is empty");
		}
		
		return linkedList.get(0);
	}

	@Override
	public T pop() {
		if(isEmpty()) {
			throw new RuntimeException("stack is empty");
		}
		
		return linkedList.remove(0);
	}

}
