package club.worldbang.structures.linkedList.doubleLinked;

/**
 * 双链表结点
 */
public class DNode<T> {
	// 数据域
	public T data;
	// 前继指针
	public DNode<T> prev;
	// 后继指针
	public DNode<T> next;

	public DNode(T data, DNode<T> prev, DNode<T> next) {
		this.data = data;
		this.prev = prev;
		this.next = next;
	}

	public DNode(T data) {
		this(data, null, null);
	}

	public DNode() {
		this(null, null, null);
	}

	public String toString() {
		return this.data.toString();
	}

}
