package club.worldbang.structures.linkedList;

/**
 * 顺序表顶级接口
 * 线性表的顺序存储结构称之为顺序表(Sequential List),
 * 它使用一维数组依次存放从a0到an-1的数据元素(a0,a1,…,an-1)， 将ai(0< i < n-1)存放在数组的第i个元素，使得ai与其前驱ai-1及后继ai+1的存储位置相邻，
 * 因此数据元素在内存的物理存储次序反映了线性表数据元素之间的逻辑次序。
 */
public interface ISeqList<T> {

	/**
	 * 判断链表是否为空
	 */
	boolean isEmpty();

	/**
	 * 链表长度
	 */
	int length();

	/**
	 * 获取元素
	 */
	T get(int index);

	/**
	 * 设置某个结点的的值
	 */
	T set(int index, T data);

	/**
	 * 根据index添加结点
	 */
	boolean add(int index, T data);

	/**
	 * 添加结点
	 */
	boolean add(T data);

	/**
	 * 根据index移除结点
	 */
	T remove(int index);

	/**
	 * 根据data移除结点
	 */
	boolean remove(T data);

	/**
	 * 根据data移除所有结点
	 */
	boolean removeAll(T data);

	/**
	 * 清空链表
	 */
	void clear();

	/**
	 * 是否包含data结点
	 */
	boolean contains(T data);

	/**
	 * 根据值查询下标
	 */
	int indexOf(T data);

	/**
	 * 根据data值查询最后一个出现在顺序表中的下标
	 */
	int lastIndexOf(T data);

}
