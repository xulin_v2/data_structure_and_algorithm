package club.worldbang.structures.linkedList.myCollection;

public interface IList<T> {

	int size();

	boolean isEmpty();

	boolean contains(T data);

	void clear();

	T get(int index);

	T set(int index, T data);

	boolean add(T data);

	void add(int index, T data);

	boolean remove(T data);

	T remove(int index);

	int indexOf(T data);

	int lastIndexOf(T data);

}
