package club.worldbang.structures.linkedList.singleLinked;

/**
 * 单链表节点，一个最基本的存储单元
 * @param <T>
 */
public class Node<T> {
	public T data;// 数据域
	public Node<T> next;// 地址域

	public Node() {

	}

	public Node(T data) {
		this.data = data;
	}

	public Node(T data, Node<T> next) {
		this.data = data;
		this.next = next;
	}

}